# README #

### What is this repository for? ###

* This repo contains some practice problems to traverse a query parse tree.
* 1.0

### What is a parse tree? ###
A parse tree is generated from a user query, which may contain important semantic information.

#### Example #1: COVID and lockdown ####
This query indicates that the user is looking for any content that must contain both words "COVID" and "lockdown".
We already have a program in place to translate this user query into a tree data structure we call a "parse tree".
Each node of the tree is a particular type of Query. Each Query contains a field called "value" and a list called "children".
The generated tree for the above query would look like this:
    
                  AndQuery
                    ("")
                 /        \
         WordQuery      WordQuery
         ("COVID")     ("lockdown")  


#### Example #2: "spring boot" or (Java and C++) ####
The generated tree for the above query would look like this:
    
                   OrQuery
                    ("")
                 /        \
         PhraseQuery       AndQuery
       ("spring boot")        ("") 
                            /      \
                     WordQuery      WordQuery
                     ("Java")       ("C++")        

#### Example #3: UK election ####
The generated tree for the above query would look like this:
    
                                               NaturalQuery
                                                  ("")
                                             /           \
                            EquivalentsQuery              WordQuery
    ("UK#United Kingdom#Britain#England#u.k.#uk")         ("election")  

### What's in it? ###

* This is a simple Java gradle project
* There is no external dependencies
* It uses JUNIT 4 for testing

### Author Info ###
Ying Andrews
