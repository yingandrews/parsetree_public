package yingandrews.parstree;

import org.junit.Test;

import java.util.LinkedHashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class ParseTreeUtilTest {

	/**
	 * User Query: UK election
	 *
	 * Parse tree before dedupe:
	 *                                           NaturalQuery
	 *                                               ("")
	 *                                          /           \
	 *                         EquivalentsQuery              WordQuery
	 * ("UK#United Kingdom#Britain#England#u.k.#uk")         ("election")
	 *
	 * Parse tree after dedupe:
	 *                                           NaturalQuery
	 *                                               ("")
	 *                                          /           \
	 *                         EquivalentsQuery              WordQuery
	 *        ("uk#united kingdom#britain#england")         ("election")
	 */
	@Test
	public void testEquivalentsDedupe() {
		Query parseTree = new AndQuery();
		EquivalentsQuery equivalentsQuery = new EquivalentsQuery();
		equivalentsQuery.setValue("UK#United Kingdom#Britain#England#u.k.#uk");
		WordQuery wordQuery = new WordQuery();
		wordQuery.setValue("election");
		parseTree.getChildren().add(equivalentsQuery);
		parseTree.getChildren().add(wordQuery);
		ParseTreeUtil.equivalentsDeDupe(parseTree);
		assertFalse(parseTree.getChildren().isEmpty());
		// The following statement should be true after you have properly implemented the ParseTreeUtil.equivalentsDeDupe() method.
//		assertEquals("uk#united kingdom#britain#england", parseTree.getChildren().get(0).getValue());
		Query nullParseTree = null;
		ParseTreeUtil.equivalentsDeDupe(nullParseTree);
		assertNull(nullParseTree);
	}

}