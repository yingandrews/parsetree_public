package yingandrews.parstree;

import org.junit.Test;

import java.util.Collections;

import static org.junit.Assert.*;

public class PhraseQueryTest {
	@Test
	public void testGetterAndSetter(){
		PhraseQuery phraseQuery = new PhraseQuery();
		assertEquals("", phraseQuery.getValue());

		phraseQuery.setValue(null);
		assertEquals("", phraseQuery.getValue());

		phraseQuery.setValue("dummy value");
		assertEquals("dummy value", phraseQuery.getValue());
	}

	@Test
	public void testGetChildren() {
		PhraseQuery naturalQuery = new PhraseQuery();
		assertEquals(Collections.EMPTY_LIST, naturalQuery.getChildren());
	}
}