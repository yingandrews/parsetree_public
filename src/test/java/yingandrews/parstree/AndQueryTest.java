package yingandrews.parstree;

import org.junit.Test;

import java.util.Collections;

import static org.junit.Assert.*;

public class AndQueryTest {
	@Test
	public void testGetterAndSetter(){
		AndQuery andQuery = new AndQuery();
		assertEquals("", andQuery.getValue());

		andQuery.setValue("dummy value");
		assertEquals("", andQuery.getValue());
	}

	@Test
	public void testGetChildren() {
		AndQuery andQuery = new AndQuery();
		assertEquals(Collections.EMPTY_LIST, andQuery.getChildren());
	}
}