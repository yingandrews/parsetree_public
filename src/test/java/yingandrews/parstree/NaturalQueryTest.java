package yingandrews.parstree;

import org.junit.Test;

import java.util.Collections;

import static org.junit.Assert.*;

public class NaturalQueryTest {
	@Test
	public void testGetterAndSetter(){
		NaturalQuery naturalQuery = new NaturalQuery();
		assertEquals("", naturalQuery.getValue());

		naturalQuery.setValue(null);
		assertEquals("", naturalQuery.getValue());

		naturalQuery.setValue("dummy value");
		assertEquals("dummy value", naturalQuery.getValue());
	}

	@Test
	public void testGetChildren() {
		NaturalQuery naturalQuery = new NaturalQuery();
		assertEquals(Collections.EMPTY_LIST, naturalQuery.getChildren());
	}

}