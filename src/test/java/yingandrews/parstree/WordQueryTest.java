package yingandrews.parstree;

import org.junit.Test;

import java.util.Collections;

import static org.junit.Assert.*;

public class WordQueryTest {

	@Test
	public void testGetterAndSetter(){
		WordQuery wordQuery = new WordQuery();
		assertEquals("", wordQuery.getValue());

		wordQuery.setValue(null);
		assertEquals("", wordQuery.getValue());

		wordQuery.setValue("dummy value");
		assertEquals("dummy value", wordQuery.getValue());
	}

	@Test
	public void testGetChildren() {
		WordQuery wordQuery = new WordQuery();
		assertEquals(Collections.EMPTY_LIST, wordQuery.getChildren());
	}

}