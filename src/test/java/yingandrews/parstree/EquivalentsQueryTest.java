package yingandrews.parstree;

import org.junit.Test;

import java.util.Collections;

import static org.junit.Assert.*;

public class EquivalentsQueryTest {
	@Test
	public void testGetterAndSetter(){
		EquivalentsQuery equivalentsQuery = new EquivalentsQuery();
		assertEquals("", equivalentsQuery.getValue());

		equivalentsQuery.setValue(null);
		assertEquals("", equivalentsQuery.getValue());

		equivalentsQuery.setValue("dummy value");
		assertEquals("dummy value", equivalentsQuery.getValue());

		assertEquals("#", equivalentsQuery.getDelimiter());

	}

	@Test
	public void testGetChildren() {
		EquivalentsQuery naturalQuery = new EquivalentsQuery();
		assertEquals(Collections.EMPTY_LIST, naturalQuery.getChildren());
	}

}