package yingandrews.parstree;

import java.util.ArrayList;
import java.util.List;

/**
 * AndQuery has no value. It's a helper structure
 * to organize other queries. It indicates the
 * relationship among its children
 */
public class AndQuery implements Query {
	private String value = "";
	private List<Query> childern = new ArrayList<>();

	@Override
	public String getValue() {
		return value;
	}

	@Override
	public void setValue(String value) {
		// do nothing
	}

	@Override
	public List<Query> getChildren() {
		return this.childern;
	}
}
