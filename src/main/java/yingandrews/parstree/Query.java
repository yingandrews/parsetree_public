package yingandrews.parstree;

import java.util.List;

public interface Query {
	String getValue();
	void setValue(String value);
	List<Query> getChildren();
}
