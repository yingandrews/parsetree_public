package yingandrews.parstree;

import java.util.ArrayList;
import java.util.List;

public class EquivalentsQuery implements Query {
	private String value = "";
	private List<Query> children = new ArrayList<>();
	private String delimiter = "#";

	@Override
	public String getValue() {
		return this.value;
	}

	@Override
	public void setValue(String value) {
		this.value = value == null ? "" : value;
	}

	@Override
	public List<Query> getChildren() {
		return this.children;
	}

	public String getDelimiter() {
		return this.delimiter;
	}
}
